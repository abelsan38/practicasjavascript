let btncalcular = document.getElementById('btnCalcular');

btncalcular.addEventListener('click', () => {
    let peso = document.getElementById('peso').value;
    let edad = document.getElementById('edad').value;
    let altura = document.getElementById('altura').value;
    let res = document.getElementById('res');
    let resultado = peso / (altura ** 2);
    resultado = resultado.toFixed(2);
    res.innerHTML = `<p>Tu IMC es de ${resultado}</p>`;
    let blur = document.getElementById('center');
    let generos = document.querySelector('input[name="sexo"]:checked').value;


    if (generos == "h") {
        cargaImagen(resultado, peso);
    } else {
        cargaImagenm(resultado, peso);
    }


    let n = 0;
    if (resultado < 18.5) {
        blur.style.boxShadow = `0px 2px 200px 0px rgba(255,255,200,1)`;
    }
    else if (resultado > 18.5 && resultado < 24.9) {
        blur.style.boxShadow = `0px 2px 200px 0px rgba(255,255,0,1)`;
    }
    else if (resultado > 24.9 && resultado < 29.9) {
        blur.style.boxShadow = `0px 2px 200px 0px rgba(255,148,0,1)`;
    } else if (resultado > 29.9 && resultado < 34.9) {
        blur.style.boxShadow = `0px 2px 200px 0px rgba(0,0,0,1)`;

    } else if (resultado > 35 && resultado < 39.9) {
        blur.style.boxShadow = `0px 2px 200px 0px rgba(255,30,0,1)`;
    } else {
        blur.style.boxShadow = `0px 2px 200px 0px rgba(255,0,0,1)`;
    }





});
let img = document.getElementById('imagen');
let limpiar = document.getElementById('btnLimpiar');
limpiar.addEventListener('click', () => {
    document.getElementById('altura').value = "";
    document.getElementById('peso').value = "";
    document.getElementById('resultado').value = "";

});

function cargaImagen(imc, peso) {

    if (imc < 18.5) {
        img.src = "../img/01H.png";
    }
    else if (imc > 18.5 && imc < 24.9) {
        img.src = "../img/02H.png";
    }
    else if (imc > 24.9 && imc < 29.9) {
        img.src = "../img/03H.png";
    } else if (imc > 29.9 && imc < 34.9) {
        img.src = "../img/04H.png";
    } else if (imc > 35 && imc < 39.9) {
        img.src = "../img/05H.png";
    } else {
        img.src = "../img/06H.png";
    }
    if (edad >= 10 && edad <= 18) {
        let cals = ((17.686 * peso) + 658).toFixed(2);
        res2.innerHTML = `<p>Las calorias por dia : ${cals}</p>`;
    } else if (edad >= 18 && edad <= 30) {
        let cals = ((15.057 * peso) + 692.2).toFixed(2);
        res2.innerHTML = `<p>Las calorias por dia : ${cals}</p>`;
    }
    else if (edad >= 30 && edad <= 60) {
        let cals = ((11.472 * peso) + 893.1).toFixed(2);
        res2.innerHTML = `<p>Las calorias por dia : ${cals}</p>`;
    } else {
        let cals = ((11.711 * peso) + 587.7).toFixed(2);
        res2.innerHTML = `<p>Las calorias por dia : ${cals}</p>`;
    }

}
function cargaImagenm(imc, peso) {

    if (imc < 18.5) {
        img.src = "../img/01M.png";
    }
    else if (imc > 18.5 && imc < 24.9) {
        img.src = "../img/02M.png";
    }
    else if (imc > 24.9 && imc < 29.9) {
        img.src = "../img/03M.png";
    } else if (imc > 29.9 && imc < 34.9) {
        img.src = "../img/04M.png";
    } else if (imc > 35 && imc < 39.9) {
        img.src = "../img/05M.png";
    } else {
        img.src = "../img/06M.png";
    }
    if (edad >= 10 && edad <= 18) {
        let cals = ((13.384 * peso) + 692.6).toFixed(2);
        res2.innerHTML = `<p>Las calorias por dia : ${cals}</p>`;
    } else if (edad >= 18 && edad <= 30) {
        let cals = ((14.818 * peso) + 486.6).toFixed(2);
        res2.innerHTML = `<p>Las calorias por dia : ${cals}</p>`;
    }
    else if (edad >= 30 && edad <= 60) {
        let cals = ((8.126 * peso) + 845.6).toFixed(2);
        res2.innerHTML = `<p>Las calorias por dia : ${cals}</p>`;
    } else {
        let cals = ((9.082 * peso) + 658.5).toFixed(2);
        res2.innerHTML = `<p>Las calorias por dia : ${cals}</p>`;
    }

}
let res2 = document.getElementById('res2');
function calorias(edad) {

}