document.getElementById("mostrarTabla").addEventListener("click", function () {
    const tablaSelect = document.getElementById("tablaSelect");
    const tablaContainer = document.getElementById("tablaContainer");
    const selectedTable = parseInt(tablaSelect.value);

    tablaContainer.innerHTML = '';
    for (let i = 1; i <= 10; i++) {
        const resultado = selectedTable * i;
        const cell = document.createElement("div");
        cell.classList.add("table-cell");

        const xImage = document.createElement("img");
        xImage.classList.add("digit");
        xImage.src = "/img/" + selectedTable + ".png";
        cell.appendChild(xImage);

        const multiplyImage = document.createElement("img"); // Para el operador "x"
        multiplyImage.classList.add("digit");
        multiplyImage.src = "/img/x.png";
        cell.appendChild(multiplyImage);

        for (let digit of i.toString()) {
            const digitImage = document.createElement("img");
            digitImage.classList.add("digit");
            digitImage.src = '/img/' + digit + ".png";
            cell.appendChild(digitImage);
        }

        const equalImage = document.createElement("img");
        equalImage.classList.add("digit");
        equalImage.src = "/img/=.png";
        cell.appendChild(equalImage);

        for (let digit of resultado.toString()) {
            const digitImage = document.createElement("img");
            digitImage.classList.add("digit");
            digitImage.src = '/img/' + digit + ".png";
            cell.appendChild(digitImage);
        }







        tablaContainer.appendChild(cell);
    }
});