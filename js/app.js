//obetenr el objetoo boton 
const calcular = document.getElementById('btnCalcular');
calcular.addEventListener('click', function () {
    let valorAuto = document.getElementById('txtValorAuto').value;
    let porcentaje = document.getElementById('txtPorcentaje').value;
    let plazo = document.getElementById('plazos').value;

    //hacer los calculos
    let pagoInicial = valorAuto * (porcentaje / 100);
    let totalFin = valorAuto - pagoInicial;
    let plazos = totalFin / plazo;

    //Mostrar datos
    document.getElementById('pagoInicial').value = pagoInicial;
    document.getElementById('txtTotalFin').value = totalFin;
    document.getElementById('txtPagoMensual').value = plazos;

});
//codificar el boton de limpiar
const limpiar = document.getElementById('btnLimpiar');
limpiar.addEventListener('click', () => {
    document.getElementById('txtValorAuto').value = "";
    document.getElementById('txtPorcentaje').value = "";
    document.getElementById('plazos').value = "";
    document.getElementById('pagoInicial').value = "";
    document.getElementById('txtTotalFin').value = "";
    document.getElementById('txtPagoMensual').value = "";
});
